'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Organisations extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Organisations.init({
    name: DataTypes.STRING,
    yearOfEstablishment: DataTypes.INTEGER,
    noOfEmployees: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Organisations',
  });
  return Organisations;
};