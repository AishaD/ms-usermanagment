'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserOrg extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  UserOrg.init({
    user_id: DataTypes.INTEGER,
    organisation_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'UserOrg',
  });
  return UserOrg;
};