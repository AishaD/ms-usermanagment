'use strict'

const prodFunctions = require("../controller/prodcontroller");

module.exports = [
    {method: 'POST', path: '/create/product', config: prodFunctions.createProduct},
    {method: 'GET', path: '/get/product', config: prodFunctions.getProducts},
    {method: 'PUT', path: '/update/product', config: prodFunctions.updateProduct},
    {method: 'DELETE', path: '/delete/product', config: prodFunctions.deleteProduct}
]