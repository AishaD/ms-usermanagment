'use strict'

const rolesController = require("../controller/rolescontroller");

module.exports = [
    {method: 'POST', path: '/create/role', config: rolesController.createRole},
    {method: 'GET', path: '/get/roles', config: rolesController.getRole},
    {method: 'PUT', path: '/update/role', config: rolesController.updateRole},
    {method: 'DELETE', path: '/delete/role', config: rolesController.deleteRole}
]