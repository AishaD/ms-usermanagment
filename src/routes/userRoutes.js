'use strict'

const userFunction = require("../controller/usercontroller")

module.exports = [
    {method: 'POST', path: '/create/user', config: userFunction.signUp},
    {method: 'POST', path: '/signin', config: userFunction.login},
    {method: 'GET', path: '/signout', config: userFunction.logout},
    {method: 'PUT', path: '/update/user', config: userFunction.updateUser},
    {method: 'GET', path: '/get/users', config: userFunction.getUser},
    {method: 'DELETE', path: '/delete/user', config: userFunction.deleteUser}
]
