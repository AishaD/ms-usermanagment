'use strict'


const s3 = require("../controller/s3bucketcontroller")

module.exports = [
    {method: 'POST', path: '/upload/s3', config: s3.uploadFile},
    {method: 'PUT', path: '/download/s3', config: s3.downloadFile}
//   {method: 'DELETE', path: '/delete/s3', config: s3.delete}
]