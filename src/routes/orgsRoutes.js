'use strict'

const orgsController = require("../controller/orgscontroller");

module.exports = [
  {method: 'POST', path: '/create/org', config: orgsController.createOrg},
  {method: 'GET', path: '/get/orgs', config: orgsController.getOrg},
  {method: 'PUT', path: '/update/org', config: orgsController.updateOrg},
  {method: 'DELETE', path: '/delete/org', config: orgsController.deleteOrg}
]