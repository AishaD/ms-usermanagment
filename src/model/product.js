var mongoose  = require('mongoose');
//const {ObjectId} = mongoose.Schema;

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },
    description: {
        type: String,
        trim: true,
        required:  true
    },
    photo: {
        data: Buffer,
        contentType: String
    }
}, { timestamps: true});


module.exports = mongoose.model("Product", productSchema);