class Session {
    constructor() {
      this.userData = {};
      this.sessionID = '';
    }
   
    set(obj) {
      this.userData = obj;
    }
    save(client){
      if(this.sessionID){
        client.set(this.sessionID, JSON.stringify(this.userData));
        client.expire(this.sessionID, 60 * 60 * 2);
      }
    }
    get(client){
      console.log(client.get(this.sessionID))
    }
    destroy(client) {
     // client.unset(this.sessionID);
      client.delete(this.sessionID)
    }
  }
module.exports = Session;


