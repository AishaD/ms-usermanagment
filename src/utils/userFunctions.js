'use strict'

const User = require("../models/user")


function verifyUniqueUser(req, res) {
    // Find an entry from the database that
    // matches either the email or username
    console.log("Verify unique")
    User.findOne({
      $or: [
        { email: req.payload.email },
        { username: req.payload.username }
      ]
    }, (err, user) => {
      // Check whether the username or email
      // is already taken and error out if so
      if (user) {
        if (user.username === req.payload.username) {
            console.log('Username taken')
        }
        if (user.email === req.payload.email) {
            console.log('Email id taken')
        }
      }
      // If everything checks out, send the payload through
      // to the route handler
      res(req.payload);
    });
    return 'Authenticated'
  }
  

  function verifyCredentials(req, res) {

    const password = req.payload.password;
  
    // Find an entry from the database that
    // matches either the email or username
    User.findOne({
      $or: [
        { email: req.payload.email },
        { username: req.payload.username }
      ]
    }, (err, user) => {
      if (user) {
        bcrypt.compare(password, user.password, (err, isValid) => {
          if (isValid) {
            res(user);
          }
          else {
            res(Boom.badRequest('Incorrect password!'));
          }
        });
      } else {
        res(Boom.badRequest('Incorrect username or email!'));
      }
    });
  }
  
  module.exports = {
    verifyUniqueUser: verifyUniqueUser,
    verifyCredentials: verifyCredentials
  }