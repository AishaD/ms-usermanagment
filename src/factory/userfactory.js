'use strict'

//const User = require("../models/user");

const db = require("../models/index")
const User = db.User;
const UserOrg = db.UserOrg;
const UserRoles = db.UserRoles;
const Organisation = db.Organisations;
const Roles = db.Roles;
const { QueryTypes, json } = require('sequelize');
const sequelize = require('../../servers/node_server');
//const redis = require('redis')
const jwt = require('jsonwebtoken')
const server = require("../../servers/node_server")
const Session = require("../utils/session");
const newSession = new Session();
const config = require('../config/config.json')
const redisClient = require('../../servers/redisclient')  
const Redis = require('ioredis')
const redis = new Redis();


const {hashPassword} = require("../utils/authUtils");
//const { RedisClient } = require("redis");
//const client = require("../../servers/redisclient");
const user = require("../models/user");


exports.userSignup = (request, h) => {

    let newId;
    let orgs;
    let roles;
    let password;
    password = hashPassword(request.payload.password);
    let name = request.payload.organisation_name;
    let rolename = request.payload.role_name;


    let user = User.create({
        firstName: request.payload.firstName,
        lastName: request.payload.lastName,
        email: request.payload.email,
        password: password,
    })
    .then((newUser) => {
        newId = newUser.id;
        console.log(newUser.get())
        console.log('New User' + newUser.id);
        // Fetch Organisation id from given name
        Organisation.findOne({attributes: ['id'], where: { name: name}})
        .then( org => {
            orgs = org.get({ plain: true})

            // Insert Values in UserOrg table
            let userOrg = UserOrg.create({
                user_id: newUser.id,
                organisation_id: orgs.id
            })
            .then((newUserOrg) => {
                console.log('New User and Organisation relation created' + newUserOrg.get())
            })
            .catch((err) => {
                console.log("Error while creation", err);
            })
        }) 
        
        // Fetch Role id from given role name
        Roles.findOne({attributes: ['id'], where: { name: rolename}})
        .then( role => {
            roles = role.get({ plain: true})

            // Insert Values in UserOrg table
            let userRoles = UserRoles.create({
                user_id: newUser.id,
                role_id: roles.id
            })
            .then((newUserRole) => {
                console.log('New User and Role relation created' + newUserRole.get())
                return json({
                    id: user._id,
                    name: user.firstName
                });
            })
            .catch((err) => {
                console.log("Error while creation", err);
            })
        }) 
        
        return json({
            id: user._id,
            name: user.firstName,
            age: 2
        });
    })
    .catch((err) => {
        console.log("Error while creation", err);
    });    
  /*  return json({
        id: user._id,
        name: user.firstName,
        age: 23
    });*/
    return 'User created'
}

exports.userLogin = (request, h) => {

    let password = request.payload.password;
    const email = request.payload.email;
    let token, tokenData;
    
    /*
    
    const comparePassword = (password) => {
        let flag = 0;
        console.log("PASSWORD CHECKING")
        password = hashPassword(password)
        if(User.findOne({attributes: [
            'email',
            'password'
        ], where: {email, password}})) {
            flag = 1
         //   console.log('FLAG' + flag)
            return flag;
        }
        else
        return flag;
    };

    
    User.findOne({ where: { email } }).then((user) => {
        if (!user) return (null, false, { message: `There is no record of the email ${email}.` });
        tokenData = {
            firstName: user.firstName,
            lastName: user.lastName,
            userEmail: user.email,
        }

    })

    
    if (!comparePassword(password)){
        return(null, false, { message: 'Your email/password combination is incorrect.' });
    } else {
        
            */
            token = jwt.sign({ _id: user._id}, email)
            console.log(token)

            new Session(tokenData, 'user')
            newSession.set("user");
            newSession.save("user");
        //    console.log(newSession.get("user"))

       /*     client.set(token, JSON.stringify(tokenData), (err,token) => {
                if(err)
                throw Error("Cannot set redis value")
                else
                console.log(token)
            })
            console.log('Token' + client.get((token, err) => {
                if(err){
                    console.log("redis value not set")
                }
                else{

                    console.log(token)
                }
            }))
*/
            const date = new Date();
            const userData = {
                email: email,
                token: token,
                timestamp: date.getTime()
            }

            redis.set(token, userData)

            redis.get(token, function (err, result) {
                if (err) {
                  console.error(err);
                } else {
                  console.log(result); // Promise resolves to "bar"
                }
              });
              
            redisClient.set(token,JSON.stringify(userData), function(err) {
                if (err) {
                // Something went wrong

                console.error(err);
                } else {
                    redisClient.get(token, function(err, value) {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log("Worked: " + value);
                        }
                    });
                }
            });      
            
            const redisData = redisClient.get(token);
            return(null, false, { token: token, message: 'Your email/password combination is CORRECT.' });
    //}
}

exports.userLogout = (request, h) => {
    
 //   newSession.destroy("user")
    return 'User logged out!!'
}

exports.getUsers = () => {
    console.log(User.findAll())
    return User.findAll({attributes: [
        'id', 'firstName', 'lastName', 'email'
    ]});
}

exports.updateUser = async(request, h) => {
    
    let oldpassword = request.payload.password;
    let oldEmail = request.payload.email;
    let newLastName = request.payload.lastName;

    oldpassword = hashPassword(oldpassword)
   
    await User.update({ lastName: newLastName }, {
        where: {
          email: oldEmail,
          password: oldpassword
        }
      });
    
      return(null, false, { message: 'User updated' });
}

exports.deleteUser = (request, h) => {
    
    User.destroy({
        where: {
            id: request.payload.id
        }
    })
    return 'User deleted'
}

