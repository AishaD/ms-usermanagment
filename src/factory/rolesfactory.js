'use strict'

const db = require("../models/index")
const Role = db.Roles;
exports.createRole = (request, response) => {

    let role = Role.create({
        name: request.payload.name,
    })
    .then((newRole) => {
        console.log('New Role' + newRole.get())
    })
    .catch((err) => {
        console.log("Error while creation", err);
    })

    return 'Role'
}

exports.getRoles = (request, response) => {
  //  console.log(Role.findAll())
  return Role.findAll();
}

exports.updateRole = async(request, response) => {
    
    let newName = request.payload.newName;
    let oldName = request.payload.oldName
   
    await Role.update({ name: newName }, {
        where: {
            name: oldName
        }
      });
    
      return(null, false, { message: 'Role updated' });
}

exports.deleteRole = (request, response) => {
    
    Role.destroy({
        where: {
            name: request.payload.name
        }
    })

    
}

