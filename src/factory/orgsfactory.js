'use strict'

const db = require("../models/index")
const Organisation = db.Organisations;


exports.createOrg = (request, response) => {

    let organisation = Organisation.create({
        name: request.payload.name,
        yearOfEstablishment: request.payload.yearOfEstablishment,
        noOfEmployees: request.payload.noOfEmployees,
    })
    .then((newOrganisation) => {
        console.log('New Organisation' + newOrganisation.get())
    })
    .catch((err) => {
        console.log("Error while creation", err);
    })
    return 'organisation';
}

exports.getAllOrgs = (request, response) => {
    // console.log(Organisation.findAll())
    return Organisation.findAll();
}

exports.updateOrg = async(request, response) => {
    
    let newName = request.payload.newName;
    let oldName = request.payload.oldName

    await Organisation.update({ name: newName }, {
        where: {
            name: oldName
        }
      });
    
      return(null, false, { message: 'Organisation updated' });
}

exports.deleteOrg = (request, response) => {
    
    Organisation.destroy({
        where: {
            name: request.payload.name
        }
    })
}

