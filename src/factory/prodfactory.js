'use strict'

const Product = require("../model/product");
const formidable = require("formidable");
const fs = require("fs");
const { error } = require("console");

exports.createProduct = async(request, h) => {

    console.log("Product creation!")
    const data = {
        name: request.payload["name"],
        description: request.payload["description"],
        photo: request.payload["file"]
    }
       const product = new Product({
        
        name: request.payload["name"],
        description: request.payload["description"],
        photo: request.payload["file"]
        
       });
    //   console.log(request.payload.name, request.payload.description)
/*
    Product.bulkWrite([
        {
            insertOne: {
                name: request.payload.name,
                description: request.payload.description,
                photo: request.payload.file
            }
        }
    ])
    */
    product.save((function (err) {
           if (err){
            console.log(err)
            return;
        }
        return product;
        /*  return json({
            name: product.name,
            description: product.description
        })*/
       }))
       return product;    
}
    

exports.getProducts = async(request, response) => {
  
try {
    var product = await Product.find().exec();
    return (product);
} catch (error) {
    return console.error(error);
}
}

exports.updateProduct = async(request, response) => {
    
    let newName = request.payload.newName;
    let oldName = request.payload.oldName
   
    await Product.updateOne({ name: newName }, {
        where: {
            name: oldName
        }
      });
    
      return(null, false, { message: 'Product updated' });
}

exports.deleteProduct = (request, response) => {
    
    Product.destroy({
        where: {
            name: request.payload.name
        }
    })

    
}

