'use strict'
const AWS = require('aws-sdk');
const fs = require('fs');


exports.uploadFile = (request, h) => {

    const data = request.payload;
        console.log(data)
        if (data.file) {
            const name = data.file.hapi.filename;
            const path = __dirname + "/uploads/" + name;
            const file = fs.createWriteStream(path);
           // const fileContent = fs.readFileSync(name);
            const fileData = fs.createReadStream('./uploads/' + name)
            //console.log(fileData)
            var fileInfo = ''

            try {
                fileInfo = fs.readFileSync('./uploads/' + name, 'utf8')
              //  console.log("File Info: " + fileInfo)
              } catch (err) {
                console.error(err)
              }

            
            const bucket = data.bucket;
            const folder = data.folder;
            const keyName = folder + '/' + name;
            const access = data.access;
            const secret = data.secret;
            console.log(bucket)
            console.log(folder)
            console.log(access)
            console.log(secret)
            console.log(keyName)


            const params = {
                Bucket: bucket,      
                Key: keyName,
                Body: fileInfo            
            };
        
            const s3 = new AWS.S3({
                accessKeyId: access,
                secretAccessKey: secret,
                bucketName: bucket     
              });

            
            file.on('error', (err) => console.error(err));

            data.file.pipe(file);

            data.file.on('end', (err) => { 
                const ret = {
                    filename: data.file.hapi.filename,
                    headers: data.file.hapi.headers
                }


            return new Promise((resolve, reject) => {
                s3.createBucket({
                    Bucket: bucket      
                }, function () {
                    s3.putObject(params, function (err, data) {
                        if (err) {
                            reject(err)
                        } else {
                             console.log("Successfully uploaded data to bucket");
                            resolve(data);
                        }
                    });
                });
            });
                return JSON.stringify(ret);
            })
        }
    return 'Product image uploaded !'
}

exports.downloadFile = (request, h) => {

    const data = request.payload;
    console.log(data)

    if (data.file) {
    
        const localDest = data.localDest;
        console.log(localDest)
    //  const name = data.file.hapi.filename;
        const name = data.file;
        const bucket = data.bucket;
        const folder = data.folder;
        const keyName = folder + '/' + name;
        const access = data.access;
        const secret = data.secret;

        let paramss = {
            Bucket: bucket,
            Key: keyName
        };

        const s3 = new AWS.S3({
            accessKeyId: access,
            secretAccessKey: secret,
            bucketName: bucket     
          });
          
       // const path =  localDest + '/' + name;
        const path = __dirname + '/uploads/' + name
        console.log(path)
        let fileData = fs.createWriteStream(path);
        //console.log(fileData)

        return new Promise((resolve, reject) => {
            s3.getObject(paramss).createReadStream()
            .on('end', () => {
                return resolve();
            })
            .on('error', (error) => {
                return reject(error);
            }).pipe(fileData);
        });
        return 'OK'
    }
    
    return 'File Downloaded !'
}

//require("../../uploads")