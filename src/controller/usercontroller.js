'use strict'

const {
    userSignup, 
    userLogin, 
    userLogout,
    getUsers,
    updateUser,
    deleteUser
} = require("../factory/userfactory")

const client = require("../../servers/redisclient")
const {responseFunction} = require("../utils/response");
const config = require("../../config/msg.json");
const validation = require("../utils/userAuthenticate");
const { verifyUniqueUser } = require("../utils/userFunctions");
const Joi = require("@hapi/joi");

//USER CREATION
exports.signUp = {   
    auth: 'default',
    tags: ['api', 'User Sign up'],
    description: 'User Sign Up',
    notes: 'Sign Up',
    validate: {
      //  payload: verifyUniqueUser
    },
    handler: (request, h) => {
      //  console.log("Authorization - " + request.auth.credentials.user)
       return responseFunction(config.usercreated, config.status, userSignup(request))
      // return userSignup(request)
    } 
};

//USER LOGIN
exports.login =  {
//    auth: 'default',
    tags: ['api', 'User Login'],
    description: 'User Login',
    notes: 'Login',
    validate: {
        payload: validation
    },
    handler: async(request, h) => {


    /*    request.cookieAuth.set({
            loggedIn: true,
            loggedInUser: request.payload.email
        })
        
        console.log(request.auth.credentials.loggedInUser)
    */
       // h.response('hello').state('user', 'logged in', {encoding: 'none'})
       // console.log(request.state.user)
        return responseFunction(config.userlogin, config.status, await userLogin(request))
    } 
};

//USER LOGOUT
exports.logout = {
    auth: 'standard',
    tags: ['api', 'User Sign up'],
    description: 'User Sign Up',
    notes: 'Sign Up',
    handler: (request, h) => {
       // h.response('Bye').unstate('user');
       console.log(request.auth.credentials.loggedInUser)

       request.cookieAuth.clear();
       console.log(request.auth.credentials.loggedInUser)

        return responseFunction(config.userlogout, config.status, userLogout(request))
    } 
};

// ADMIN CAN VIEW ALL USERS' DATA
exports.getUser = {
    auth: 'default',
    tags: ['api', 'Get All Users'],
    description: 'Get All Users',
    notes: 'Get All Users',
    handler: async() => {
        const result = responseFunction(config.getuser, config.status, await getUsers());
        console.log(result)
        return result;
    }
};

//ADMIN CAN UPDATE USER DATA
exports.updateUser = {
    auth: 'default',
    tags: ['api', 'Update User'],
    description: 'Update User',
    notes: 'Update User',
    validate: {
        payload: Joi.object({
            lastName: Joi.string().required()
        })
    },
    handler: async(request, h) => {
        return responseFunction(config.userupdate, config.status, await updateUser(request))
    } 
};

//ADMIN CAN DELETE USER
exports.deleteUser = {
    auth: 'default',
    tags: ['api', 'Delete User'],
    description: 'Delete User',
    notes: 'Delete User',
    validate: {
        payload: Joi.object({
            id: Joi.number().required()
        })
    },
    handler: (request, h) => {

        return responseFunction(config.userdeleted, config.status, deleteUser(request))
    } 
};
