'use strict'

//Load the SDK and UUID
var AWS = require('aws-sdk');
var uuid = require('uuid');
var fs = require('fs')
const Hapi = require('@hapi/hapi');
const { request } = require('http');
const { responseFunction } = require("../utils/response")
const config = require("../../config/msg.json")
const {uploadFile, downloadFile} = require("../factory/s3bucketfactory")


//UPLOAD ROUTER
exports.uploadFile = {
    description: 'Upload API',
    notes: 'File Uploading',
    tags: ['api', 'Upload'],   
    payload: {
        output: 'stream',
        parse: true,
        allow: 'multipart/form-data'
    },    
    handler: (request, h) => {
    console.log("Upload handler")
    return responseFunction(config.s3upload, config.status, uploadFile(request))
    }
};  
      
    
//DOWNLOAD ROUTER
exports.downloadFile = ({
    description: 'Download API',
    notes: 'File Download',
    tags: ['api', 'Download'],        
    handler: (request, h) => {
        return responseFunction(config.s3download, config.status, downloadFile(request))
    }       
});  
       

/*
//DELETE ROUTER
exports.delete = ({
    description: 'Delete API',
    notes: 'File Delete',
    tags: ['api', 'Delete'],
    handler: (req, h) => {
          
        return deleteFile(req);
    }
});
*/


   