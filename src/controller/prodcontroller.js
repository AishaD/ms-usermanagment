'use strict'

const {
    createProduct, 
    getProducts, 
    updateProduct,
    deleteProduct,
} = require("../factory/prodfactory")

const {responseFunction} = require("../utils/response");
const config = require("../../config/msg.json");

//CREATE PRODUCT
exports.createProduct = {
//    auth: 'default',
    tags: ['api', 'Create Product'],
    description: 'Create Product',
    notes: 'Create Product',
    payload: {
        multipart: {
            output: 'stream'
        },
      //  allow: ['multipart/form-data']
    },
    handler: async(request, h) => {
        console.log("Hello")
        return responseFunction(config.prodcreated, config.status, await createProduct(request))
        
    }
};

// VIEW ALL PRODUCT DETAILS
exports.getProducts = {
//    auth: 'default',
    tags: ['api', 'Get All Products'],
    description: 'Get All Products',
    notes: 'All Products',
    handler: async() => {
        return responseFunction(config.getprods, config.status, await getProducts())  
    }
};


//UPDATE PRODUCT DETAILS
exports.updateProduct = {
//    auth: 'default',
    tags: ['api', 'Update Product'],
    description: 'Update Product',
    notes: 'Update Product',
    handler: async(request, h) => {
        return responseFunction(config.produpdate, config.status, await updateProduct(request))  
    }
};


//DELETE ANY PRODUCT
exports.deleteProduct = {
//    auth: 'default',
    tags: ['api', 'Delete Product'],
    description: 'Delete Product',
    notes: 'Delete Product',
    handler: (request, h) => {
        return responseFunction(config.proddelete, config.status, deleteProduct(request))  
    }
};
