'use strict'

const {
    createOrg, 
    getAllOrgs, 
    updateOrg,
    deleteOrg,
} = require("../factory/orgsfactory");

const {responseFunction} = require("../utils/response");
const config = require("../../config/msg.json");

// CREATE ORGANISATION
exports.createOrg = {
    auth: 'default',
    tags: ['api', 'Create Organization'],
    description: 'Create Organization',
    notes: 'Create Organization',
    handler: (request, h) => {
       return responseFunction(config.orgcreated, config.status, createOrg(request))
    } 
};

// VIEW ALL ORGANISATION DATA
exports.getOrg =  {
    auth: 'default',
    tags: ['api', 'Get All Organizations'],
    description: 'All Organization',
    notes: 'All Organization',
    handler: async() => {
        return responseFunction(config.getorg, config.status, await getAllOrgs())       
    }
};

// UPDATE ORGANISATION DATA
exports.updateOrg = {
    auth: 'default',
    tags: ['api', 'Update Organization'],
    description: 'Update Organization',
    notes: 'Update Organization',
    handler: async(request, h) => {
        return responseFunction(config.orgupdate, config.status, await updateOrg(request))       
    }
};

// DELETE ANY ORGANISATION
exports.deleteOrg = {
    auth: 'default',
    tags: ['api', 'Delete Organization'],
    description: 'Delete Organization',
    notes: 'Delete Organization',
    handler: (request, h) => {
        return responseFunction(config.orgdeleted, config.status, deleteOrg(request))       
    }
};
