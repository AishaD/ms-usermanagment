'use strict'

const {
    createRole, 
    getRoles, 
    updateRole,
    deleteRole,
} = require("../factory/rolesfactory")

const {responseFunction} = require("../utils/response");
const config = require("../../config/msg.json");

//CREATE ROLE
exports.createRole = {
    auth: 'default',
    tags: ['api', 'Create Role'],
    description: 'Create Role',
    notes: 'Create Role',
    handler: (request, h) => {
        return responseFunction(config.rolecreated, config.status, createRole(request))
        
    }
};

// VIEW ALL ROLE DETAILS
exports.getRole = {
    auth: 'default',
    tags: ['api', 'Get All Roles'],
    description: 'Get All Role',
    notes: 'All Role',
    handler: async() => {
        return responseFunction(config.getroles, config.status, await getRoles())  
    }
};


//UPDATE ROLE DETAILS
exports.updateRole = {
    auth: 'default',
    tags: ['api', 'Update Role'],
    description: 'Update Role',
    notes: 'Update Role',
    handler: async(request, h) => {
        return responseFunction(config.roleupdate, config.status, await updateRole(request))  
    }
};


//DELETE ANY ROLE
exports.deleteRole = {
    auth: 'default',
    tags: ['api', 'Delete Role'],
    description: 'Delete Role',
    notes: 'Delete Role',
    handler: (request, h) => {
        return responseFunction(config.roledeleted, config.status, deleteRole(request))  
    }
};
