'use strict';

const Hapi = require('@hapi/hapi');
const mongoose = require('mongoose');
const config = require('../config/config.json');



// const mongoDbUri = 'mongodb://' + config.database.host + '/' + config.database.db;
let mongoDbUri = "";

if(config.env == 'preprod') {
mongoDbUri = 'mongodb://' + config.mongodb.username + ':' + config.mongodb.password + '@' + config.mongodb.host + ':' + config.mongodb.port + '/' + config.mongodb.db + '?authSource=admin';
} else if (config.env == 'development') {
mongoDbUri = 'mongodb://' + config.mongodb.username + ':' + config.mongodb.password + '@' + config.mongodb.host + ':' + config.mongodb.port + '/' + config.mongodb.db + '?authSource=admin';
} else {
mongoDbUri = "mongodb://" + config.mongodb.host + ":" + config.mongodb.port + "/" + config.mongodb.db;
}

console.log('URL is ' + mongoDbUri)

mongoose.connect(mongoDbUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
    reconnectTries: 30,
    reconnectInterval: 100000,
    bufferMaxEntries: 0
});


mongoose.connection.on('connected', () => {
console.log(`app is connected to ${mongoDbUri}`);
});
mongoose.connection.on('error', err => {
// console.log('error while connecting to mongodb', err);
});

module.exports = mongoose;

