'use strict'

const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');
const {resolve} = require('path');
const config = require("../config/config.json")
const redis = require('redis')
var redisClient = redis.createClient({
    port      : 6379,               
    host      : '120.0.0.1',       
    password  : config.password,    
});
const basic = require('hapi-auth-basic')
const Boom = require('boom')
const Auth = require('hapi-auth-bearer-token')
const cookie = require('hapi-auth-cookie')
const userSchema = require("../src/utils/userAuthenticate")
require("../servers/redisclient")

const userRoute = require("../src/routes/userRoutes")
const orgsRoutes = require('../src/routes/orgsRoutes');
const roleRoutes = require('../src/routes/roleRoutes');
const prodRoutes = require('../src/routes/prodRoutes');
const s3Route = require('../src/routes/s3bucketRoutes')
const client = require('./redisclient');


//SCHEME FOR ADMIN AUTHORIZATION
const scheme = function (server, options) {

    return {
        authenticate: function (request, h) {

			const req = request.raw.req;
			console.log(request.payload);
			const authorization = req.headers.authorization;
			const scope = req.headers.scope;
            if (!authorization || scope != 1 ) {
                throw Boom.unauthorized(null, 'Custom', {message: "ACCESS DENIED !!!"});
			}
			redisClient.set('authorization', authorization)
			redisClient.expire('authorization', 1000) //10 minutes

			console.log('REDIS CLIENT ', redisClient.get('authorization'))

			
            return h.authenticated({ credentials: { user: 'Authenticated' } });
        }
    };
};


const swaggerOptions = {
	info: {
		title: 'User Management',
		description: 'User Management',
		version: '1.0',
	},
	schemes: ["http", "https"],
	tags: [
		{
			name: 'User Management',
			description: 'Api tasks interface.'
		},
	],
//	basePath: "/utility",
	swaggerUI: true,
	documentationPage: true,
	host: config.host,
	documentationPath: '/docs',
	templates: resolve('public', 'templates')

}

let server_connection_options = {
	host: config.node.host,
	port: config.node.port,
	state: {
		strictHeader: false,
		ignoreErrors: true
	},
	routes: {
		cors: true,
		timeout: {
			server: 1200000, // 1,200,000 or 20 minutes
			socket: 1300000
		}
	}
};

const server = Hapi.server(server_connection_options);
server.auth.scheme('custom', scheme);
server.auth.strategy('default', 'custom');

// COOKIE REGISTERATION
server.register(require('hapi-auth-cookie'));
server.auth.strategy('standard','cookie', {

	cookie: {
		password:  'secretpasswordnotrevealedtoanyone',
		isSecure: false,
		ttl: 24 * 60 * 60 * 1000
	}
	
})
server.register(require('hapi-auth-bearer-token'));
server.register([
	Inert,
	Vision,
	{
		plugin: HapiSwagger,
		options: swaggerOptions
	},
	
]).then(async () => {
	server.route(userRoute);
	server.route(orgsRoutes);
	server.route(roleRoutes);
	server.route(prodRoutes);
	server.route(s3Route);



    await start();
})


// Start the server
async function start() {
    try {
        await server.start();
    } catch (err) {
        console.log(err);
        process.exit(1);
    }

    console.log('Server running at:', server.info.uri);
}




